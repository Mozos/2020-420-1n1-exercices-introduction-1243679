package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class Test_serie10_Math extends SimulConsole {
    @Test
    @Order(1)
    void test_math1() throws Exception {
        choixMenu("10a");
        ecrire("3 4");
        assertSortie("3", false);

        choixMenu("10a");
        ecrire("4 3");
        assertSortie("3", false);


    }

    @Test
    @Order(2)
    void test_math2() throws Exception {
        choixMenu("10b");
        ecrire("-3 4");
        assertSortie("4", false);

        choixMenu("10b");
        ecrire("-4 3");
        assertSortie("4", false);

        choixMenu("10b");
        ecrire("-4 -3");
        assertSortie("4", false);
    }
}
