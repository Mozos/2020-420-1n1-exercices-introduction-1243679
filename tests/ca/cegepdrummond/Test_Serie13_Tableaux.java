package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie13_Tableaux extends SimulConsole {
    @Test
    @Order(1)
    void test_tableau1() throws Exception {
        choixMenu("13a");
        ecrire("5 5 4 3 2 1");
        assertSortieMultiLignes("5 4 3 2 1");

        choixMenu(".");
        ecrire("5 10 4 3 5 6");
        assertSortieMultiLignes("10 4 3 5 6");
    }

    @Test
    @Order(2)
    void test_tableau2() throws Exception {
        choixMenu("13b");
        ecrire("6 5 4 7 3 10 12");
        assertSortie("12", false);
    
        choixMenu(".");
        ecrire("3 6 5 4");
        assertSortie("6", false);
    }

    @Test
    @Order(3)
    void test_tableau3() throws Exception {
        choixMenu("13c");
        ecrire("5 1 2 3 4 5");
        assertSortie("5 4 3 2 1 ", false);

        choixMenu(".");
        ecrire("4 1 2 3 4");
        assertSortie("4 3 2 1 ", false);


    }
    
    @Test
    @Order(4)
    void test_tableau4() throws Exception {
        choixMenu("13d");
        ecrire("5 2 2 2 2 2");
        assertSortie("10", false);
        
        choixMenu(".");
        ecrire("6 1 1 1 1 1 1");
        assertSortie("6", false);
        
    }
    
    @Test
    @Order(5)
    void test_tableau5() throws Exception {
        choixMenu("13e");
        ecrire("5 1 2 3 4 5");
        ecrire("5 1 2 3 4 5");
        assertSortie("2 4 6 8 10 ", false);
        
        choixMenu(".");
        ecrire("4 4 4 4 4");
        ecrire("4 5 5 5 5");
        assertSortie("9 9 9 9 ", false);
    }
    
    @Test
    @Order(6)
    void test_tictactoe() throws Exception {
        choixMenu("13f");
        assertSortieMultiLignes("123 456 789");
        assertSortie("position pour les O",false);
        ecrire("1");
        assertSortieMultiLignes("O23 456 789");
        assertSortie("position pour les X", false);
        ecrire("2");
        assertSortieMultiLignes("OX3 456 789");
        assertSortie("position pour les O",false);
        ecrire("3");
        assertSortieMultiLignes("OXO 456 789");
        assertSortie("position pour les X", false);
        ecrire("4");
        assertSortieMultiLignes("OXO X56 789");
        assertSortie("position pour les O",false);
        ecrire("5");
        assertSortieMultiLignes("OXO XO6 789");
        assertSortie("position pour les X", false);
        ecrire("6");
        assertSortieMultiLignes("OXO XOX 789");
        assertSortie("position pour les O",false);
        ecrire("7");
        assertSortieMultiLignes("OXO XOX O89");
        assertSortie("position pour les X", false);
        ecrire("8");
        assertSortieMultiLignes("OXO XOX OX9");
        assertSortie("position pour les O",false);
        ecrire("9");
        assertSortieMultiLignes("OXO XOX OXO");

        assertSortie("position pour les X", false);
        ecrire("0");
    }

}
