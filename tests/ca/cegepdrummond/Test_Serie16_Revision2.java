package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie16_Revision2 extends SimulConsole {

    @Test
    @Order(1)
    void test_revision1() {
        choixMenu("16a");
        ecrire("La plateforme Java");
        ecrire("est adaptative");
        assertSortie("La", false);
        assertSortie("plateforme", false);
        assertSortie("Java", false);
        assertSortie("est", false);
        assertSortie("adaptative", false);

        choixMenu(".");
        ecrire("Ce cours est tres interessant");
        assertSortie("Ce", false);
        assertSortie("cours", false);
        assertSortie("est", false);
        assertSortie("tres", false);
        assertSortie("interessant", false);
    }

    @Test
    @Order(2)
    void test_revision2() {
        choixMenu("16b");
        ecrire("10 3 2");
        assertSortie("8", false);

        choixMenu(".");
        ecrire("15 5 3");
        assertSortie("6", false);

        choixMenu(".");
        ecrire("8 3 1");
        assertSortie("4", false);
    }

    @Test
    @Order(3)
    void test_revision3() {
        choixMenu("16c");
        ecrire("1000 1000 1000");
        assertSortie("4", false);

        choixMenu(".");
        ecrire("5000 100 100");
        assertSortie("5", false);

        choixMenu(".");
        ecrire("5000 500 95");
        assertSortie("6", false);
    }

    @Test
    @Order(4)
    void test_revision4() {
        choixMenu("16d");
        ecrire("8 16");
        assertSortie("8", false);
        assertSortie("Fizz", false);
        assertSortie("Buzz", false);
        assertSortie("11", false);
        assertSortie("Fizz", false);
        assertSortie("13", false);
        assertSortie("14", false);
        assertSortie("FizzBuzz", false);
        assertSortie("16", false);
    }

    @Test
    @Order(5)
    void test_revision5() {
        choixMenu("16e");
        ecrire("17");
        assertSortie("17 52 26 13 40 20 10 5 16 8 4 2 1", false);

        choixMenu(".");
        ecrire("1");
        assertSortie("1", false);

        choixMenu(".");
        ecrire("12");
        assertSortie("12 6 3 10 5 16 8 4 2 1", false);
    }

    @Test
    @Order(6)
    void test_revision6() {
        choixMenu("16f");
        ecrire("80000000");
        assertSortie("22h13m20s 0ms", false);

        choixMenu(".");
        ecrire("2500135");
        assertSortie("0h41m40s 135ms", false);

        choixMenu(".");
        ecrire("3800254");
        assertSortie("1h03m20s 254ms", false);

        choixMenu(".");
        ecrire("3780888");
        assertSortie("1h03m00s 888ms", false);
    }
}
