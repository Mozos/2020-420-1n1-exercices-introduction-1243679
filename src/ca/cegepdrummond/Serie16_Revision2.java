package ca.cegepdrummond;

import java.util.Scanner;

public class Serie16_Revision2 {

    /* Écrivez un algorithme qui lit 5 mots et les output sur des lignes differentes.
     * Indice: Utiliser un tableau pour mettre vos mots.
     */
    public void revision1() {
        // Votre code
    }

    /* Résoudre le problème suivant:
     * Un escargot monte un poteau the p hauteur.
     * De jour, il monte de m hauteur.
     * De nuit, il descend de d hauteur.
     * En combien de jour atteint t'il le haut du poteau?
     */
    public void revision2() {
        Scanner scanner = new Scanner(System.in);

        int p = scanner.nextInt();
        int m = scanner.nextInt();
        int d = scanner.nextInt();

        // Votre code

        System.out.println(/* ??? */);
    }

    /* Une machine a cafe produit du cafe selon les valeurs entree dans la console.
     * Elle a X eau, X lait et x grain de cafe
     * Elle utilise 250 eau, 20 lait et 15 grain de cafe
     * Selon les valeurs entree, combien peut-elle produire de cafe?
     */
    public void revision3() {
        Scanner scanner = new Scanner(System.in);

        int eau = scanner.nextInt();
        int lait = scanner.nextInt();
        int grain = scanner.nextInt();

        // Votre code

        System.out.println(/* ??? */);
    }

    /* Fizz Buzz est un problème classic de programmation. Voici comment il marche:
     * Le programme lit deux nombres entiers, le debut de l'interval et la fin, inclusivement.
     * Le programme "output" tous les nombres de cette interval, mais si le nombre est divisible par 3, il output Fizz
     * Aussi, si le nombre est divisible par 5, il output Buzz. Finalement, si le nombre est divisible par 3 et 5,
     * le programme output FizzBuzz.
     *
     * Afficher chaque nombre ou mot sur une nouvelle ligne.
     */
    public void revision4() {
        Scanner scanner = new Scanner(System.in);

        int debut = scanner.nextInt();
        int fin = scanner.nextInt();

        // Votre code
    }

    /* Probleme: sequence de int decrite dans la conjecture Collatz
     * Etant donné un nombre entier n, créez la sequence appropriée
     * Si n est pair, divisez le par 2. Si il est impaire, multiplier le par 3 et ajouter 1
     * Répéter jusqu'à ce que vous obteniez 1.
     * Afficher la sequence sur une seule ligne séparée par des espaces.
     */
    public void revision5() {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        // Votre code
    }

    /* Etant donné un certain nombre de milisecondes, transformer celui-ci en affichage d'heure
     * Exemple de output "1h32m25s 155ms"
     * Indice: Utiliser la division entière et le modulo
     */
    public void revision6() {
        Scanner scanner = new Scanner(System.in);

        long timeMS = scanner.nextLong();

        // Votre code
    }
}
