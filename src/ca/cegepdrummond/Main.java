package ca.cegepdrummond;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Serie1_Introduction serie1 = new Serie1_Introduction();
        Serie2_Variables serie2 = new Serie2_Variables();
        Serie3_OperateursEtConditions serie3 = new Serie3_OperateursEtConditions();
        Serie4_EntreesClavier serie4 = new Serie4_EntreesClavier();
        Serie5_OperateursBooleens serie5 = new Serie5_OperateursBooleens();
        Serie6_String serie6 = new Serie6_String();
        Serie7_Boucles serie7 = new Serie7_Boucles();
        Serie10_Math serie10 = new Serie10_Math();
        Serie11_Fonctions serie11 = new Serie11_Fonctions();
        Serie13_Tableaux serie13 = new Serie13_Tableaux();
        Serie14_Pseudocode serie14 = new Serie14_Pseudocode();
        Serie15_Revision serie15 = new Serie15_Revision();
        Serie16_Revision2 serie16 = new Serie16_Revision2();

        String choix = "";
        String choixPrecedent = "";
        Scanner scanner = new Scanner(System.in);
        while (!choix.equals("0")) {
            choixPrecedent = choix;
            System.out.println("Quel exercice désirez-vous exécuter? (0 pour terminer) ");
            choix = scanner.nextLine();
            if (choix.equals(".")) {
                choix = choixPrecedent;
            }
            switch (choix.trim()) {
                case "0" -> System.out.println("bye");

                case "1a" -> serie1.HelloWorld();
                case "1b" -> serie1.print1();
                case "1c" -> serie1.print2();
                case "1d" -> serie1.print3();

                case "2a" -> serie2.variable1();
                case "2b" -> serie2.variable2();
                case "2c" -> serie2.variable3();
                case "2d" -> serie2.variable4();

                case "3a" -> serie3.operateur1();
                case "3b" -> serie3.operateur2();
                case "3c" -> serie3.operateur3();
                case "3d" -> serie3.operateur4();
                case "3e" -> serie3.operateur5();
                case "3f" -> serie3.operateur6();
                case "3g" -> serie3.condition1();
                case "3h" -> serie3.condition2();
                case "3i" -> serie3.condition3();

                case "4a" -> serie4.clavier1();
                case "4b" -> serie4.clavier2();
                case "4c" -> serie4.clavier3();
                case "4d" -> serie4.clavier4();
                case "4e" -> serie4.clavier5();
                case "4f" -> serie4.clavier6();

                case "5a" -> serie5.bool1();
                case "5b" -> serie5.bool2();
                case "5c" -> serie5.bool3();
                case "5d" -> serie5.bool4();
                case "5e" -> serie5.bool5();
                case "5f" -> serie5.bool6();
                case "5g" -> serie5.bool7();
                case "5h" -> serie5.boolEtClavier1();
                case "5i" -> serie5.boolEtClavier2();
                case "5j" -> serie5.boolEtClavier3();
                case "5k" -> serie5.boolEtClavier4();
                case "5l" -> serie5.boolEtClavier5();

                case "6a" -> serie6.string1();
                case "6b" -> serie6.string2();
                case "6c" -> serie6.string3();
                case "6d" -> serie6.string4();
                case "6e" -> serie6.string5();
                case "6f" -> serie6.string6();

                case "7a" -> serie7.while1();
                case "7b" -> serie7.while2();
                case "7c" -> serie7.while3();
                case "7d" -> serie7.dowhile1();
                case "7e" -> serie7.dowhile2();
                case "7f" -> serie7.dowhile3();
                case "7g" -> serie7.for1();
                case "7h" -> serie7.for2();
                case "7i" -> serie7.for3();
                case "7j" -> serie7.boucle1();
                case "7k" -> serie7.boucle2();
                case "7l" -> serie7.boucle3();
                case "7m" -> serie7.boucle4();
                case "7n" -> serie7.boucle5();

                case "10a" -> serie10.math1();
                case "10b" -> serie10.math2();

                case "11a" -> serie11.fonction1();
                case "11b" -> serie11.fonction2();
                case "11c" -> serie11.fonction3();
                case "11d" -> serie11.fonction4();
                case "11e" -> serie11.fonction5();
                case "11f" -> serie11.fonction6();
                case "11g" -> serie11.fonction7();
                case "11h" -> serie11.fonction8();
                case "11i" -> serie11.fonction9();

                case "13a" -> serie13.tableau1();
                case "13b" -> serie13.tableau2();
                case "13c" -> serie13.tableau3();
                case "13d" -> serie13.tableau4();
                case "13e" -> serie13.tableau5();
                case "13f" -> serie13.tictactoe();

                case "14a" -> serie14.pseudo1();
                case "14b" -> serie14.pseudo2();

                case "15a" -> serie15.revision1();
                case "15b" -> serie15.revision2();

                case "16a" -> serie16.revision1();
                case "16b" -> serie16.revision2();
                case "16c" -> serie16.revision3();
                case "16d" -> serie16.revision4();
                case "16e" -> serie16.revision5();
                case "16f" -> serie16.revision6();

                default -> System.out.println("choix invalide");
            }

        }
    }
}
