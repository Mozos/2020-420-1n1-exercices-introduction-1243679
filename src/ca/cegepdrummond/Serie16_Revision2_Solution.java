package ca.cegepdrummond;

import java.util.Scanner;

public class Serie16_Revision2_Solution {

    /* Écrivez un algorithme qui lit 5 mots et les output sur des lignes differentes.
     * Indice: Utiliser un tableau pour mettre vos mots.
     */
    public void revision1() {
        Scanner scanner = new Scanner(System.in);

        String[] mots = new String[5];
        for (int i = 0; i < 5; i++) {
            mots[i] = scanner.next();
        }

        // Ceci
        for (int i = 0; i < mots.length; i++) {
            System.out.println(mots[i]);
        }

        // Ou cela
        for (String mot : mots) {
            System.out.println(mot);
        }
    }

    /* Résoudre le problème suivant:
     * Un ascargot monte un poteau the p hauteur.
     * De jour, il monte de m hauteur.
     * De nuit, il descend de d hauteur.
     * En combien de jour atteint t'il le haut du poteau?
     */
    public void revision2() {
        Scanner scanner = new Scanner(System.in);

        int p = scanner.nextInt();
        int m = scanner.nextInt();
        int d = scanner.nextInt();

        int hauteur = 0;
        int jours = 0;
        while (hauteur < p) {
            hauteur += m;
            jours++;
            if (hauteur < p) {
                hauteur -= d;
            }
        }

        System.out.println(jours);
    }

    /* Une machine a cafe produit du cafe selon les valeurs entree dans la console.
     * Elle a X eau, X lait et x grain de cafe
     * Elle utilise 250 eau, 20 lait et 15 grain de cafe
     * Selon les valeurs entree, combien peut-elle produire de cafe?
     */
    public void revision3() {
        Scanner scanner = new Scanner(System.in);

        int eau = scanner.nextInt();
        int lait = scanner.nextInt();
        int grain = scanner.nextInt();

        int eauParCafe = 250;
        int laitParCafe = 20;
        int grainParCafe = 15;

        System.out.println(Math.min(eau / eauParCafe, Math.min(lait / laitParCafe, grain / grainParCafe)));
    }

    /* Fizz Buzz est un problème classic de programmation. Voici comment il marche:
     * Le programme lit deux nombres entiers, le debut de l'interval et la fin, inclusivement.
     * Le programme "output" tous les nombres de cette interval, mais si le nombre est divisible par 3, il output Fizz
     * Aussi, si le nombre est divisible par 5, il output Buzz. Finalement, si le nombre est divisible par 3 et 5,
     * le programme output FizzBuzz.
     *
     * Afficher chaque nombre ou mot sur une nouvelle ligne.
     */
    public void revision4() {
        Scanner scanner = new Scanner(System.in);

        int debut = scanner.nextInt();
        int fin = scanner.nextInt();

        for (int number = debut; number <= fin; number++) {
            if (number % 3 == 0) {
                if (number % 5 == 0) {
                    System.out.println("FizzBuzz");
                } else {
                    System.out.println("Fizz");
                }
            } else if (number % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(number);
            }
        }
    }

    /* Probleme: sequence de int decrite dans la conjecture Collatz
     * Etant donné un nombre entier n, créez la sequence appropriée
     * Si n est pair, divisez le par 2. Si il est impaire, multiplier le par 3 et ajouter 1
     * Répéter jusqu'à ce que vous obteniez 1.
     * Afficher la sequence sur une seule ligne séparée par des espaces.
     */
    public void revision5() {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        while (n != 1) {
            System.out.print(n + " ");
            if (n % 2 == 0) {
                n /= 2;
            } else {
                n *= 3;
                n++;
            }
        }
        System.out.println(n);
    }

    /* Etant donné un certain nombre de milisecondes, transformer celui-ci en affichage d'heure
     * Exemple de output "1h32m25s 155ms"
     * Indice: Utiliser la division entière et le modulo
     */
    public void revision6() {
        Scanner scanner = new Scanner(System.in);

        long timeMS = scanner.nextLong();

        int ms = (int) timeMS % 1_000;
        int s = (int) timeMS / 1_000 % 60;
        int m = (int) timeMS / 60_000 % 60;
        int h = (int) timeMS / 3_600_000;

        System.out.println(h + "h" + (m > 9 ? m + "m" : "0" + m + "m") + (s > 9 ? s + "s " : "0" + s + "s ") +
                ms + "ms");
    }
}
